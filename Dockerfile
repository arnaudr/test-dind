FROM debian:bullseye-slim AS builder
RUN apt update \
 && apt install -y bzip2 dpkg-dev wget
